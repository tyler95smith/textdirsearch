#include <string>
#include <cstring>
#include <iostream>
#include <dirent.h>
#include <fstream>
#include <algorithm>
#include <sys/stat.h>

using namespace std;

/********************************************************/
/*  ToDo                                               */
/* allow multiple search terms                        */
/* allow options to search for exact terms and regex */
/* do not check non text files such as a .jpg       */
/***************************************************/

bool is_file(const char* path) {
    struct stat buf;
    stat(path, &buf);
    return S_ISREG(buf.st_mode);
}

bool is_dir(const char* path) {
    struct stat buf;
    stat(path, &buf);
    return S_ISDIR(buf.st_mode);
}

struct Result{
    int lineNum;
    string text;
    string filename;
};

bool searchStr(string line, string search_val) {
    std::transform(line.begin(), line.end(), line.begin(), ::tolower);
    if (line.find(search_val) != std::string::npos) {
        return true;
    }
    return false;
}

bool isValidFileType(string filename)
{
    // change this to build from the ignore.txt
    // might need to also add a global ignoreTypes.txt so it isnt rebuilt every time.
    vector<string> ignoreTypes;
    ignoreTypes.push_back(".jpg");
    ignoreTypes.push_back(".png");
    
    for (int i = 0; i < (int)ignoreTypes.size();i++)
    {
        if (searchStr(filename,ignoreTypes[i]))
        {
            return false;
        }
    }
    return true;
}

vector<Result> searchFile(string filename, string search_val) {
    ifstream file;
    int lineNum = 1;
    vector<Result> results;
    Result tempRes;
    string line;
    file.open(filename,ios::in);
    
    if (file.is_open()) {
        getline(file,line);
        if (searchStr(line, search_val)) {
            //cout << "line num: " << lineNum << "; text: " << line << endl;
            tempRes.lineNum = lineNum;
            tempRes.text = line;
            tempRes.filename = filename;
            results.push_back(tempRes);
        }
        
        while(getline(file,line)) {
            lineNum++;
            if (searchStr(line, search_val)) {
                //cout << "line num: " << lineNum << "; text: " << line << endl;
                tempRes.lineNum = lineNum;
                tempRes.text = line;
                tempRes.filename = filename;
                results.push_back(tempRes);
            }
        }
    }
    
    return results;
}

vector<Result> searchDir(std::string path, string search_val) {
	struct dirent *entry;
	DIR *dp;
    vector<Result> results;
    vector<Result> temp;
    
	// Open the directory containing the data
	dp = opendir(path.c_str());
	if (dp == NULL) {
		perror("opendir");
	}

	std::string base(path);
	base += "/";
	
	while ((entry = readdir(dp))) {
		if (entry->d_name[0] != '.') { // dont search hidden files
			std::string filename = base + (std::string) entry->d_name;
			
			char* y = new char[filename.length() + 1];
			strcpy(y,filename.c_str());
			
			// check whether subject is a file or dir
			if (is_dir(y)) {
			    temp = searchDir(filename, search_val);
                results.insert(results.end(),temp.begin(),temp.end());
			} else if (isValidFileType(filename)) { // validate the file type
                temp = searchFile(filename, search_val);
                results.insert(results.end(),temp.begin(),temp.end());
			}
			
            delete[] y;
		}
	}

	closedir(dp);
	return results;
}

void printResults(vector<Result> results) {
    if ((int)results.size() > 0) {
        for (int i = 0; i < (int)results.size();i++) {
            cout << "file: " << results[i].filename << "; line num: " << results[i].lineNum << "; text: " << results[i].text << endl;
        }
    } else {
        cout << "No results found!\n";
    }
}

int main(int argc, char *argv[])
{
    if(argc >= 4)
    {
        vector<Result> results;
        string path = "";
        string searchTerm = "";
        string primeArg = argv[1];
        
        if (primeArg == "-d") // target directory
        {
            path = argv[2];
            searchTerm = argv[4];
        } else if (primeArg == "-s") // search terms
        {
            searchTerm = argv[2];
            path = argv[4];
        }
        
        results = searchDir(path,searchTerm);
        
        printResults(results);
        
    } else {
        cout << "------------------\n";
        cout << "-d dir path\n";
        cout << "-s \"search term\"\n";
        cout << "------------------\n";
    }
    
    return 0;
}


